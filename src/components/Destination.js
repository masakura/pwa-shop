export default class Destination {
  constructor(data) {
    this.data = data;
  }

  toRaw() {
    return Object.assign({}, this.data);
  }

  static fromRaw(raw) {
    return new Destination(raw);
  }

  static empty() {
    return new Destination({
      name: '',
      email: '',
      postalCode: '',
    });
  }
}
