// noinspection JSUnusedGlobalSymbols
export default class Money {
  /**
   * @param {number} value
   */
  constructor(value) {
    this.value = value;
  }

  multiply(quantity) {
    return new Money(this.value * quantity);
  }

  add(money) {
    return new Money(this.value + money.value);
  }

  toString() {
    const s = `${this.value}`.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,');
    return `${s} 円`;
  }

  static zero() {
    return new Money(0);
  }
}
