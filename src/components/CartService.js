import Money from './Money';
import Destination from './Destination';
import histories, { HistoryService } from './HistoryService';
import Product from './Product';

export class CartItem {
  constructor(item, cart) {
    this.item = item;
    this.quantity = 0;
    this.cart = cart;
    this.update();
  }

  increment() {
    this.quantity += 1;
    this.update();
  }

  decrement() {
    this.quantity -= 1;
    this.update();
  }

  sameItem(item) {
    return this.item.id === item.id;
  }

  update() {
    if (this.quantity < 0) this.quantity = 0;
    this.price = this.item.price.multiply(this.quantity);
    this.id = this.item.id;

    if (this.cart) this.cart.update();
  }

  toRaw() {
    return {
      item: new Product(this.item).toRaw(),
      quantity: this.quantity,
    };
  }

  static fromRaw(raw) {
    const item = Product.fromRaw(raw.item).data;
    const cartItem = new CartItem(item);
    cartItem.quantity = raw.quantity;
    cartItem.update();
    return cartItem;
  }
}

export class Cart {
  constructor(id, items, destination) {
    this.id = id || Math.round(Math.random() * 100000000).toString();
    this.items = items || [];
    this.destination = destination || Destination.empty();
    this.update();
  }

  find(item) {
    const filtered = this.items.filter(i => i.sameItem(item));
    if (filtered.length > 0) return filtered[0];

    const cartItem = new CartItem(item, this);
    this.items.push(cartItem);
    return cartItem;
  }

  add(item) {
    const cartItem = this.find(item);
    cartItem.increment();
    this.update();
  }

  update() {
    this.price = this.items.reduce((prev, current) => prev.add(current.price), Money.zero());
    this.quantity = this.items.reduce((prev, current) => prev + current.quantity, 0);
    this.empty = this.quantity <= 0;
  }

  toRaw() {
    const items = this.items.map(item => item.toRaw());
    return {
      id: this.id,
      items,
      destination: this.destination.toRaw(),
    };
  }

  static fromRaw(raw) {
    const items = raw.items.map(item => CartItem.fromRaw(item));
    const destination = Destination.fromRaw(raw.destination);
    return new Cart(raw.id, items, destination);
  }
}

export class CartService {
  constructor(hist) {
    this.histories = hist || new HistoryService();
    this.clear();
  }

  add(item) {
    this.current.add(item);
  }

  get empty() {
    return this.current.empty;
  }

  get price() {
    return this.current.price;
  }

  clear() {
    this.current = new Cart();
  }

  async buy(pushManager) {
    console.log(pushManager);
    const id = this.current.id; // eslint-disable-line prefer-destructuring

    await this.histories.add(this.current);
    this.clear();

    if (pushManager) {
      const push = await pushManager.subscribe();
      const data = {
        id,
        url: window.location.toString().replace(/#.*/, `#history/${id}`),
      };
      push.buy(data);
    }
  }
}

export default new CartService(histories);
