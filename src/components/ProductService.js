import Product from './Product';
import carts from './CartService';

class DummyApiClient {
  // noinspection JSMethodCanBeStatic
  fetch() { // eslint-disable-line class-methods-use-this
    return Promise.resolve([
      Product.dummy(1, 3128),
      Product.dummy(2, 123),
      Product.dummy(3, 5400),
      Product.dummy(4, 1280),
      Product.dummy(5, 100),
    ]);
  }
}

class ProductService {
  constructor() {
    this.client = new DummyApiClient();
  }

  async fetch() {
    return ProductService.products(await this.client.fetch());
  }

  static product(data) {
    return new Product(data, carts);
  }

  static products(data) {
    return data.map(prod => ProductService.product(prod));
  }
}

export default new ProductService();
