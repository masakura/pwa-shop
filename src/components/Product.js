import Money from './Money';

export default class Product {
  constructor(data, carts) {
    this.data = data;
    this.carts = carts;
  }

  addToCart() {
    this.carts.add(this.data);
  }

  toRaw() {
    return Object.assign({}, this.data, {
      price: this.data.price.value,
    });
  }

  static fromRaw(raw) {
    return new Product(Product.from(raw));
  }

  static from(data) {
    return Object.assign({}, data, {
      price: new Money(data.price),
    });
  }

  static dummy(id, price) {
    const name = `商品 ${id}`;
    const data = {
      id,
      name,
      imageUrl: `https://placehold.jp/3d4070/ffffff/150x150.png?text=${encodeURIComponent(name)}`,
      price,
    };
    return Product.from(data);
  }
}
