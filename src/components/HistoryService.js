import { Cart } from './CartService';

export class State {
  constructor(name, displayName) {
    this.name = name;
    this.displayName = displayName;
  }

  toString() {
    return this.displayName;
  }
}

State.unknown = new State('unknown', '不明');
State.waiting = new State('waiting', '発送待ち');
State.delivering = new State('delivering', '配送中');
State.complete = new State('complete', '完了');
const parse = (name) => {
  switch (name) {
    case State.waiting.name: return State.waiting;
    case State.delivering.name: return State.delivering;
    case State.complete.name: return State.complete;
    default: return State.unknown;
  }
};

State.parse = parse;

export class History {
  constructor(cart, time, state) {
    this.cart = cart;
    this.time = time || new Date();
    this.state = state || State.waiting;
  }

  get id() {
    return this.cart.id;
  }

  get price() {
    return this.cart.price;
  }

  toRaw() {
    return {
      cart: this.cart.toRaw(),
      time: this.time.getTime(),
      state: this.state.name,
    };
  }

  static fromRaw(raw) {
    const cart = Cart.fromRaw(raw.cart);

    return new History(cart, new Date(raw.time), State.parse(raw.state));
  }
}

class HistoryMemoryStorage {
  constructor() {
    this.items = [];
  }

  add(history) {
    this.items.unshift(history);
  }

  find(id) {
    const filtered = this.items.filter(i => i.id === id);
    return Promise.resolve(filtered.length > 0 ? filtered[0] : undefined);
  }

  fetch() {
    return Promise.resolve(this.items);
  }
}

class HistoryIndexedDBStroage {
  constructor() {
    const request = indexedDB.open('shop', 100);
    request.addEventListener('upgradeneeded', (event) => {
      const db = event.target.result;
      db.createObjectStore('histories', { keyPath: 'id' });
    });
    request.addEventListener('success', (event) => {
      const db = event.target.result;
      db.close();
    });
  }

  fetch() { // eslint-disable-line class-methods-use-this
    const request = indexedDB.open('shop');

    return new Promise((resolve) => {
      request.addEventListener('success', (event) => {
        const db = event.target.result;

        const store = db.transaction(['histories'], 'readonly').objectStore('histories');
        const result = store.getAll();

        result.addEventListener('success', (ev) => {
          resolve(HistoryIndexedDBStroage.fromRaw(ev.target.result));
        });
      });
    });
  }

  add(history) { // eslint-disable-line class-methods-use-this
    const raw = history.toRaw();
    raw.id = history.id;

    const request = indexedDB.open('shop');

    return new Promise((resolve) => {
      request.addEventListener('success', (event) => {
        const db = event.target.result;

        const store = db.transaction(['histories'], 'readwrite').objectStore('histories');
        const result = store.put(raw);

        result.addEventListener('success', () => {
          resolve();
        });
      });
    });
  }

  find(id) { // eslint-disable-line class-methods-use-this
    const request = indexedDB.open('shop');

    return new Promise((resolve) => {
      request.addEventListener('success', (event) => {
        const db = event.target.result;

        const store = db.transaction(['histories'], 'readonly').objectStore('histories');
        const result = store.get(id);

        result.addEventListener('success', (ev) => {
          console.log(ev.target.result);
          resolve(History.fromRaw(ev.target.result));
        });
      });
    });
  }

  static fromRaw(raw) {
    return raw.map(line => History.fromRaw(line));
  }
}

export class HistoryService {
  constructor() {
    this.provider = window.indexedDB ? new HistoryIndexedDBStroage() : new HistoryMemoryStorage();
    this.items = [];
  }

  async fetch() {
    if (this.isFetching) return;
    this.isFetching = true;

    const items = await this.provider.fetch();
    this.items = items;

    this.isFetching = false;
    console.log(this.items);
  }

  async add(cart) {
    await this.provider.add(new History(cart));
    await this.fetch();
  }

  find(id) {
    return this.provider.find(id);
  }

  static create2() {
    if (HistoryService.instance) return HistoryService.instance;

    const instance = new HistoryService();
    HistoryService.instance = instance;

    instance.fetch();
    return instance;
  }
}

const instance = HistoryService.create2();

export default instance;
