class Client {
  constructor(base = 'https://pwa-push.azurewebsites.net') {
    this.base = base;
  }

  async publicKey() {
    const response = await fetch(`${this.base}/keys/public`);
    const key = (await response.json()).publicKey;
    return this.toBinary(key);
  }

  async buy(subscription, data) {
    const message = Object.assign(
      { data },
      JSON.parse(JSON.stringify(subscription)),
    );
    await fetch(`${this.base}/buy`, {
      method: 'POST',
      body: JSON.stringify(message),
      headers: {
        'Accept': 'application/json', // eslint-disable-line quote-props
        'Content-Type': 'application/json',
      },
    });
  }

  toBinary(base64) { // eslint-disable-line class-methods-use-this
    const data = base64.replace(/-/g, '+').replace(/_/g, '/');
    const raw = window.atob(data);
    const binary = new Uint8Array(raw.length);
    for (let i = 0; i < raw.length; i++) { // eslint-disable-line no-plusplus
      binary[i] = raw.charCodeAt(i);
    }
    return binary;
  }
}

class Push {
  constructor(subscription) {
    this.subscription = subscription;
    this.client = new Client();
  }

  buy(data) {
    this.client.buy(this.subscription, data);
  }
}

export class PushManager {
  constructor(registration) {
    this.pushManager = registration.pushManager;
    this.client = new Client();
  }

  async subscribe() {
    const subscription = (await this.getRegistration()) || (await this.register());
    return new Push(subscription);
  }

  getRegistration() {
    return this.pushManager.getSubscription();
  }

  async register() {
    const key = await this.client.publicKey();
    const subscription = await this.pushManager.subscribe({
      userVisibleOnly: true,
      applicationServerKey: key,
    });
    return subscription;
  }

  static create(registration) {
    if (PushManager.instance) return PushManager.instance;

    if (!registration) return undefined;

    const instance = new PushManager(registration);
    PushManager.instance = instance;
    return instance;
  }
}

export default {};
