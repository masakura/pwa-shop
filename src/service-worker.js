/* eslint-disable no-restricted-globals */

const urlsToCache = self.__precacheManifest // eslint-disable-line no-underscore-dangle
  .map(item => item.url);

class Storage {
  async find(id) { // eslint-disable-line class-methods-use-this
    const request = indexedDB.open('shop', 100);

    return new Promise((resolve) => {
      request.addEventListener('success', (event) => {
        const db = event.target.result;

        const store = db.transaction(['histories'], 'readonly').objectStore('histories');
        const result = store.get(id);

        result.addEventListener('success', ev => resolve(ev.target.result));
      });
    });
  }

  async update(id, state) {
    const history = await this.find(id);
    history.state = state;

    const request = indexedDB.open('shop', 100);

    return new Promise((resolve) => {
      request.addEventListener('success', (event) => {
        const db = event.target.result;

        const store = db.transaction(['histories'], 'readwrite').objectStore('histories');
        const result = store.put(history);

        result.addEventListener('success', () => resolve());
      });
    });
  }
}

self.addEventListener('install', (event) => {
  const invoke = async () => {
    const cache = await caches.open('pwa-shop-cache');
    await cache.addAll(urlsToCache);
  };

  event.waitUntil(invoke());
});

self.addEventListener('fetch', (event) => {
  const request = event.request; // eslint-disable-line prefer-destructuring

  const invoke = async () => (await caches.match(request)) ||
    (await fetch(request)); // eslint-disable-line no-return-await

  event.respondWith(invoke());
});

self.addEventListener('push', (event) => {
  const message = event.data.json();

  const invoke = async () => {
    await self.registration.showNotification(message.title, message);

    const storage = new Storage();
    await storage.update(message.data.id, message.data.state);

    const result = await self.clients.matchAll({});
    result.forEach(client => client.postMessage('update'));
  };

  event.waitUntil(invoke());
});

self.addEventListener('notificationclick', (event) => {
  const invoke = async (notification) => {
    notification.close();

    await self.clients.openWindow(notification.data.url);
  };

  event.waitUntil(invoke(event.notification));
});
