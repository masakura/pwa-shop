import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import CartPage from './views/CartPage.vue';
import DestinationPage from './views/DestinationPage.vue';
import PaymentPage from './views/PaymentPage.vue';
import FinishPage from './views/FinishPage.vue';
import HistoryPage from './views/HistoryPage.vue';
import HistoryDetailPage from './views/HistoryDetailPage.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/cart',
      name: 'cart',
      component: CartPage,
    },
    {
      path: '/destination',
      name: 'destination',
      component: DestinationPage,
    },
    {
      path: '/payment',
      name: 'payment',
      component: PaymentPage,
    },
    {
      path: '/finish/:id',
      name: 'finish',
      component: FinishPage,
      props: true,
    },
    {
      path: '/history',
      name: 'history',
      component: HistoryPage,
    },
    {
      path: '/history/:id',
      name: 'history-detail',
      component: HistoryDetailPage,
      props: true,
    },
  ],
});
