import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import fontawesome from '@fortawesome/fontawesome';
import brands from '@fortawesome/fontawesome-free-brands';
import solid from '@fortawesome/fontawesome-free-solid';
import faSpinner from '@fortawesome/fontawesome-free-solid/faSpinner';
import App from './App.vue';
import router from './router';
import './registerServiceWorker';

Vue.config.productionTip = false;

Vue.use(BootstrapVue);
fontawesome.library.add(brands, solid, faSpinner);

// noinspection JSUnusedGlobalSymbols
new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
