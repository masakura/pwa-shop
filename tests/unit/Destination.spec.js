import { expect } from 'chai';
import Destination from '@/components/Destination';

describe('Destination', () => {
  let target;

  beforeEach(() => {
    target = new Destination({
      name: 'taro',
      email: 'foo@example.com',
      postalCode: '000-0000',
    });
  });

  it('json', () => {
    const result = Destination.fromRaw(JSON.parse(JSON.stringify(target.toRaw())));

    expect(result.data).to.deep.equal(target.data);
  });
});
