import { expect } from 'chai';
import { History, State } from '@/components/HistoryService';
import { Cart } from '@/components/CartService';
import Product from '@/components/Product';

describe('CartService', () => {
  const item1 = Product.dummy(1, 123);
  const item2 = Product.dummy(2, 50);
  let target;

  beforeEach(() => {
    const cart = new Cart();
    cart.add(item1);
    cart.add(item1);
    cart.add(item2);
    cart.destination.data.name = 'taro';

    target = new History(cart);
    target.state = State.delivering;
  });

  it('json', () => {
    const result = History.fromRaw(JSON.parse(JSON.stringify(target.toRaw())));

    expect(result.id).to.equal(target.id);
    expect(result.time.getTime()).to.equal(target.time.getTime());
    expect(result.state.name).to.equal(State.delivering.name);
  });
});
