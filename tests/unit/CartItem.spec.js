import { expect } from 'chai';
import { CartItem } from '@/components/CartService';
import Product from '@/components/Product';

describe('CartItem', () => {
  const item1 = Product.dummy(1, 123);
  let target;

  beforeEach(() => {
    target = new CartItem(item1);
    target.quantity = 3;
  });

  it('json', () => {
    const result = CartItem.fromRaw(JSON.parse(JSON.stringify(target.toRaw())));

    expect(result.item.id).to.equal(1);
    expect(result.quantity).to.equal(3);
    expect(result.price.toString()).to.equal('369 円');
  });
});
