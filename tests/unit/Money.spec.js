import { expect } from 'chai';
import Money from '@/components/Money';

describe('Money', () => {
  it('toString', () => {
    const target = new Money(1234);

    const result = `${target}`;

    expect(result).to.equal('1,234 円');
  });

  it('multiply', () => {
    const target = new Money(123);

    const result = target.multiply(3);

    expect(result).to.deep.equal({ value: 369 });
  });

  it('add', () => {
    const target = new Money(123);
    const target2 = new Money(100);

    const result = target.add(target2);

    expect(result).to.deep.equal({ value: 223 });
  });
});
