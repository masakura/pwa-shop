import { expect } from 'chai';
import { Cart } from '@/components/CartService';
import Product from '@/components/Product';

describe('CartService', () => {
  const item1 = Product.dummy(1, 123);
  const item2 = Product.dummy(2, 50);
  let target;

  beforeEach(() => {
    target = new Cart();
    target.add(item1);
    target.add(item1);
    target.add(item2);
    target.destination.data.name = 'taro';
  });

  it('json', () => {
    const result = Cart.fromRaw(JSON.parse(JSON.stringify(target.toRaw())));

    expect(result.id).to.equal(target.id);
    expect(result.destination.data.name).to.equal('taro');
    expect(result.items[0].id).to.equal(1);
    expect(result.items[1].id).to.equal(2);
    expect(result.price.toString()).to.equal('296 円');
  });
});
