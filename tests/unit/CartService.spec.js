import { expect } from 'chai';
import { CartService } from '@/components/CartService';
import Product from '@/components/Product';
import { HistoryService } from '@/components/HistoryService';

describe('CartService', () => {
  const item1 = Product.from({ id: 1, price: 123 });
  let target;

  beforeEach(() => {
    target = new CartService(new HistoryService());
  });

  describe('add', () => {
    it('add one item', () => {
      target.add(item1);

      const result = target.current.find(item1);

      expect(result.item).to.deep.equal(item1);
      expect(result.quantity).to.equal(1);
    });

    it('add two same item', () => {
      target.add(item1);
      target.add(item1);

      const result = target.current.items[0];

      expect(result.item).to.deep.equal(item1);
      expect(result.quantity).to.equal(2);
    });
  });

  describe('clear', () => {
    it('Clear cart', () => {
      target.add(item1);

      target.clear();

      const result = target.empty;

      expect(result).to.be.true; // eslint-disable-line no-unused-expressions
    });
  });

  describe('empty', () => {
    it('first is empty', () => {
      const result = target.empty;

      expect(result).to.be.true; // eslint-disable-line no-unused-expressions
    });

    it('add item is not empty', () => {
      target.add(item1);

      const result = target.empty;

      expect(result).to.be.false; // eslint-disable-line no-unused-expressions
    });
  });

  describe('decrement', () => {
    it('decrement one', () => {
      target.add(item1);

      const item = target.current.find(item1);
      item.decrement();

      const result = item.quantity;

      expect(result).to.equal(0);
    });

    it('quantity greater than 0', () => {
      target.add(item1);

      const item = target.current.find(item1);
      item.decrement();
      item.decrement();
      item.decrement();

      const result = item.quantity;

      expect(result).to.equal(0);
    });
  });

  describe('CartItem.price', () => {
    it('price', () => {
      target.add(item1);
      const item = target.current.find(item1);

      const result = item.price;

      expect(result).to.deep.equal({ value: 123 });
    });
  });

  describe('price', () => {
    it('price', () => {
      target.add(item1);

      const result = target.price;

      expect(result).to.deep.equal({ value: 123 });
    });
  });

  describe('buy', () => {
    it('add to history', async () => {
      target.add(item1);
      const cart = target.current;

      await target.buy();

      const result = target.histories.items[0];

      expect(result.id).to.equal(cart.id);
    });

    it('clear cart', async () => {
      target.add(item1);
      const cart = target.current;

      await target.buy();

      const result = target.current;

      expect(result.id).to.not.equal(cart.id);
    });
  });
});
