import { expect } from 'chai';
import Product from '@/components/Product';

describe('Product', () => {
  it('dummy', () => {
    const result = Product.dummy(1, 1230);

    expect(result).to.deep.equal({
      id: 1,
      name: '商品 1',
      imageUrl: 'https://placehold.jp/3d4070/ffffff/150x150.png?text=%E5%95%86%E5%93%81%201',
      price: { value: 1230 },
    });
  });

  it('json', () => {
    const item1 = Product.dummy(1, 1230);
    const target = new Product(item1);

    const result = Product.fromRaw(JSON.parse(JSON.stringify(target.toRaw())));

    expect(result.data).to.deep.equal(item1);
    expect(result.data.price.toString()).to.equal('1,230 円');
  });
});
