describe('Home', () => {
  describe('Home', () => {
    it('ショップブラントが表示されている', () => {
      cy.visit('/');
      cy.contains('.navbar-brand', 'PWA Shop');
    });
  });

  describe('Products', () => {
    it('商品が一つ以上表示されている', () => {
      cy.visit('/');
      cy.get('.product-list .product .product-name').children().first()
        .should('not.empty');
    });
  });
});
