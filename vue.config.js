const config = {
  pwa: {
    name: 'PWA Shop',
    themeColor: '#007bff',
    msTileColor: '#000000',

    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      swSrc: 'src/service-worker.js',
    },
  },
};

if (process.env.BASE_URL) { config.baseUrl = process.env.BASE_URL; }

module.exports = config;
